#!/bin/sh

#compton &

#nitrogen --restore &

#alacritty &

#steam -silent &

feh --bg-fill ~/.config/qtile/mountain.jpg &

picom &

alacritty &

steam -silent &

xrandr --verbose --output "DP-1" --primary &

